import Vue from 'vue';

import Avatar from '@/components/Avatar';
import AppColumn from "@/components/Layout/Column";
import AppIconButton from "@/components/Button/IconButton";
import AppDropDown from '@/components/Dropdown';
import AppRow from "@/components/Layout/Row";
import { Text } from '@/components/Text';

Vue.component('Avatar', Avatar);
Vue.component('AppColumn', AppColumn);
Vue.component('AppDropdown', AppDropDown);
Vue.component('AppRow', AppRow);
Vue.component('AppText', Text);
Vue.component('AppIconButton', AppIconButton);