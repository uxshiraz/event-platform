class Toaster {
  constructor(context) {
    this.context = context;
  }

  show(message, variant, autoHide = false, position) {
    const i18nMessage = this.context.app.i18n.t(message);

    this.context.store.commit("setToast", {
      show: true,
      message: i18nMessage === message ? message : i18nMessage,
      variant,
      position: position || process.env.toasterPosition || "bottom-left",
      autoHide
    });
  }

  dismiss() {
    this.context.store.commit('setToast', {
      show: false
    });
  }

  success(message) {
    this.show(message, "success");
  }

  error(message) {
    this.show(`errors.${message}`, "danger");
  }

  httpError(status) {
    this.error(`http.${status}`);
  }
}

export default (ctx, inject) => {
  inject('toast', new Toaster(ctx))
}