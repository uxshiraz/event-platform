// import WsConnection from '../modules/ws';
import Ws from "@adonisjs/websocket-client";

export default ({ $auth, env }, inject) => {
  const ws = Ws(null, {
    path: "ws"
  });

  ws.getChannel = channel => ws.getSubscription(channel) || ws.subscribe(channel);

  if ($auth.loggedIn) {
    const token = $auth.getToken($auth.strategy.name).replace("Bearer ", "");
    ws.withJwtToken(token);

    ws.connect();
  }

  inject("ws", ws);
};
