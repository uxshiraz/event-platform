export default ({ route }, inject) => {
  inject("urls", {
    adamak: process.env.adamakUrl,

    withQueries(url) {
      const queries = Object.keys(route.query)
        .map(key => {
          return `${key}=${route.query[key]}`;
        })
        .join("&");

      return url + (queries ? `?${queries}` : "");
    },

    withRedirect(url, current = false) {
      let redirect = route.query.redirect
        ? `?redirect=${route.query.redirect}`
        : null;

      if (
        !redirect &&
        current &&
        !["/", "/signin", "/signup"].includes(route.fullPath)
      ) {
        redirect = `?redirect=${route.fullPath}`;
      }

      return url + (redirect ? redirect : "");
    },

    current() {
      return `${process.env.appUrl}${route.fullPath}`;
    }
  });
};
