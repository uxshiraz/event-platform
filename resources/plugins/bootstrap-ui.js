/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from "vue";

import {
  BAlert,
  BButton,
  BCollapse,
  BDropdown,
  BDropdownItem,
  BDropdownDivider,
  BFormRadio,
  BFormCheckbox,
  BFormRadioGroup,
  BNav,
  BNavbar,
  BNavItem,
  BNavbarBrand,
  BTooltip,
  CollapsePlugin,
  ModalPlugin,
  TooltipPlugin,
  ToastPlugin,
  BProgress
} from "bootstrap-vue";

Vue.component("BAlert", BAlert);
Vue.component("BButton", BButton);
Vue.component("BCollapse", BCollapse);
Vue.component("BDropdown", BDropdown);
Vue.component("BDropdownItem", BDropdownItem);
Vue.component("BDropdownDivider", BDropdownDivider);
Vue.component("BFormRadio", BFormRadio);
Vue.component("BFormRadioGroup", BFormRadioGroup);
Vue.component("BFormCheckbox", BFormCheckbox);
Vue.component("BNav", BNav);
Vue.component("BNavbar", BNavbar);
Vue.component("BNavItem", BNavItem);
Vue.component("BNavbarBrand", BNavbarBrand);
Vue.component("BTooltip", BTooltip);
Vue.component("BProgress", BProgress);

Vue.use(CollapsePlugin);
Vue.use(ModalPlugin)
Vue.use(TooltipPlugin);
Vue.use(ToastPlugin);