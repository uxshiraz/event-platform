/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from "vue";

import {
  BIconArchive,
  BIconArrowsFullscreen,
  BIconBack,
  BIconBell,
  BIconCircleFill,
  BIconChat,
  BIconClipboardCheck,
  BIconCameraVideo,
  BIconChatFill,
  BIconCheck,
  BIconCheckCircleFill,
  BIconChevronRight,
  BIconChevronDown,
  BIconChevronLeft,
  BIconCloudUpload,
  BIconDoorOpen,
  BIconGear,
  BIconGrid,
  BIconLink45deg,
  BIconMic,
  BIconPerson,
  BIconPersonCircle,
  BIconPersonPlusFill,
  BIconPersonLinesFill,
  BIconPencilSquare,
  BIconQuestion,
  BIconQuestionCircle,
  BIconQuestionCircleFill,
  BIconThreeDotsVertical,
  BIconX,
  BIconGeoFill,
  BIconTrash,
  BIconPeople,
  BIconPeopleFill,
  BIconVolumeUp,
  BIcon
} from "bootstrap-vue";

Vue.component("BIconArchive", BIconArchive);
Vue.component("BIconArrowsFullscreen", BIconArrowsFullscreen);
Vue.component("BIconBack", BIconBack);
Vue.component("BIconBell", BIconBell);
Vue.component('BIconCircleFill', BIconCircleFill);
Vue.component("BIconChat", BIconChat);
Vue.component("BIconClipboardCheck", BIconClipboardCheck);
Vue.component("BIconCameraVideo", BIconCameraVideo);
Vue.component("BIconChatFill", BIconChatFill);
Vue.component("BIconCheck", BIconCheck);
Vue.component("BIconCheckCircleFill", BIconCheckCircleFill);
Vue.component("BIconChevronRight", BIconChevronRight);
Vue.component("BIconChevronDown", BIconChevronDown);
Vue.component("BIconChevronLeft", BIconChevronLeft);
Vue.component("BIconCloudUpload", BIconCloudUpload);
Vue.component("BIconDoorOpen", BIconDoorOpen);
Vue.component("BIconGear", BIconGear);
Vue.component("BIconGrid", BIconGrid);
Vue.component("BIconLink45deg", BIconLink45deg);
Vue.component("BIconMic", BIconMic);
Vue.component("BIconPencilSquare", BIconPencilSquare);
Vue.component("BIconPeople", BIconPeople);
Vue.component("BIconPeopleFill", BIconPeopleFill);
Vue.component("BIconQuestion", BIconQuestion);
Vue.component("BIconQuestionCircle", BIconQuestionCircle);
Vue.component("BIconQuestionCircleFill", BIconQuestionCircleFill);
Vue.component("BIconPerson", BIconPerson);
Vue.component("BIconPersonCircle", BIconPersonCircle);
Vue.component("BIconPersonPlusFill", BIconPersonPlusFill);
Vue.component("BIconPersonLinesFill", BIconPersonLinesFill);
Vue.component("BIconThreeDotsVertical", BIconThreeDotsVertical);
Vue.component("BIconX", BIconX);
Vue.component("BIconGeoFill", BIconGeoFill);
Vue.component("BIconTrash", BIconTrash);
Vue.component("BIconVolumeUp", BIconVolumeUp);
Vue.component("BIcon", BIcon);
