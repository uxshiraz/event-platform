import Vue from "vue";
import persianJs from "persianjs";
import DOMPurify from "dompurify";

Vue.filter("en", value => {
  if (value === null || value === "") return value;

  return persianJs(value)
    .persianNumber()
    .toString();
});

Vue.filter("fn", value => {
  if (value === null || value === "") return value;

  return persianJs(value)
    .englishNumber()
    .toString();
});

Vue.filter("nl2br", value => {
  if (!value) return null;

  return value.replace(/[\n\r]+/g, "<br />");
});

Vue.filter("nl22br", value => {
  if (!value) return null;

  return value.replace(/[\n\r]+/g, "<br /><br />");
});

Vue.filter("sanitizedDomified", value => {
  if (!value) return "";

  const nl2bred = value.replace(/[\n\r]+/g, "<br />");
  const linkified = nl2bred.replace(
    /(https?:\/\/[^\s]+)/g,
    '<a href="$1" target="_blank">$1</a>'
  );
  const purified = DOMPurify.sanitize(linkified, {
    USE_PROFILES: { html: true },
    ADD_ATTR: ["target"]
  });

  return purified;
});

Vue.filter("cdn", value => {
  if (!process.env.cdnEnabled || /^https?:\/\//.test(value)) {
    return value;
  }

  const cdn = process.env.cdnUrl.replace(/\/+$/, "");
  value = value.replace(/^\/+/, "");
  return `${cdn}/${value}`;
});

Vue.filter("domain", value => {
  if (!value) return null;

  // run against regex
  const matches = value.match(/^https?:\/\/([^/?#]+)(?:[/?#]|$)/i);
  // extract hostname (will be null if no match is found)
  return matches && matches[1];
});
