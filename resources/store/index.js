const MESSAGES_MAX_LENGTH = 500;

export const state = () => ({
  locale: "fa",
  flashData: null,
  toaster: {
    show: false,
    title: null,
    message: null,
    icon: "info-circle",
    position: "bottom-left",
    variant: null,
    autoHide: false
  },
  showConf: false,
  session: null,
  isChatScrolled: false,
  pins: [
    {
      title: "پارسکدرز",
      thumb_url: "parsecoders.png",
      link: "https://parscoders.com"
    },
    {
      title: "مورف",
      thumb_url: "morph.png",
      link: "https://morphdesign.io"
    }
  ],
  onlineUsers: [],
  messages: [],
  isAllMessagesLoaded: false,
  pinnedMessage: null,
  pinnedUser: null
});

export const mutations = {
  setToast(state, data) {
    state.toaster = {
      ...state.toaster,
      ...data
    };
  },

  setShowConf(state, payload) {
    state.showConf = payload;
  },

  setFlashData(state, data) {
    state.flashData = data;
  },

  setSession(state, data) {
    state.session = data;
  },

  setPins(state, data) {
    state.pins = data;
  },

  setOnlineUsers(state, data) {
    state.onlineUsers = data;
  },

  setMessages(state, data) {
    state.messages = data;
  },

  addNewMessage(state, data) {
    if (state.messages.length > MESSAGES_MAX_LENGTH && !state.isChatScrolled) {
      state.messages.shift();
      state.isAllMessagesLoaded = false;
    }
    state.messages.push(data);
  },

  addOldMessages(state, data) {
    const { messages, end } = data;
    state.messages.unshift(...messages.reverse());
    state.isAllMessagesLoaded = end;
  },

  setPinnedMessage(state, data) {
    state.pinnedMessage = data.data;
    state.pinnedUser = data.user;
  },

  setChatScrolled(state, data) {
    state.isChatScrolled = data;
  }
};

export const actions = {
  nuxtServerInit({ commit }, { app, route }) {
    if (route.path !== "/redirect") {
      const flashData = app.$cookies.get("flash-data");

      if (flashData) {
        commit("setFlashData", flashData);
        app.$cookies.remove("flash-data");
      }
    }
  }
};
