import { findIndex } from "lodash";

export const state = () => ({
  questions: []
});

export const mutations = {
  setQuestions(state, payload) {
    const tempQuestions = [...payload];
    tempQuestions.sort(function(a, b) {
      return a.sort - b.sort;
    });
    state.questions = payload;
  },

  addQuestion(state, payload) {
    const i = findIndex(state.questions, { id: payload.id });
    if (i >= 0) return;
    state.questions.push(payload);
  },

  removeQuestion(state, payload) {
    const i = findIndex(state.questions, { id: payload.id });

    if (i >= 0) state.questions.splice(i, 1);
  },

  editQuestion(state, payload) {
    // We have to create new array instead of mutating to make QA component aware of state change!
    const tempQuestions = [...state.questions];

    const i = findIndex(tempQuestions, { id: payload.id });
    if (i >= 0) tempQuestions[i] = payload;

    state.questions = tempQuestions;
  }
};
