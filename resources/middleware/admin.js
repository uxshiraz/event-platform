export default function({ $auth, error, env }) {
  if (!$auth.loggedIn)
    return error({ statusCode: 403 })

  const users = env.rootUsers.split(",");

  if (!users.includes($auth.user._id))
    return error({ statusCode: 403 })
}