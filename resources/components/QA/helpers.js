export const SELECT_STATE = "archived";
export const DESELECT_STATE = "unArchived";
export const ANSWERED_STATE = "answered";
export const UNANSWERED_STATE = "unAnswered";
export const ARCHIVE_STATE = "destroy";
