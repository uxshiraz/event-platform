export const FULL_SCREEN_ACTION_KEY = 'FULL_SCREEN';
export const SOUND_ACTION_KEY = 'SOUND';
export const MIC_ACTION_KEY = 'MIC';
export const LIVE_ACTION_KEY = 'LIVE';