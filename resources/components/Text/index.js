import { mergeData } from "bootstrap-vue/src/vue";
import Vue from "vue";

export const props = ({
  tag: {
    type: String,
    default: "p"
  },
  size: {
    type: String,
    default: "md"
  },
  variant: {
    type: String,
    default: ""
  }
});

export const Text = Vue.extend({
  name: "AppText",
  functional: true,
  props,
  render(h, { props, data, children }) {
    const { tag } = props;

    const componentData = {
      class: [
        `text-${props.size}`,
        props.variant ? `text-${props.variant}` : ""
      ]
    };

    return h(tag, mergeData(data, componentData), children);
  }
});
