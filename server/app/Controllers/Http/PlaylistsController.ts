import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import Playlist from 'App/Models/Playlist'

export default class PlaylistsController {
  public async index({ response }: HttpContextContract) {
    const data = await Playlist.find().sort('-order')

    return response.json(data)
  }

  public async show({ request }: HttpContextContract) {

  }

  public async store({ request, response }: HttpContextContract) {
    await request.validate({
      schema: schema.create({
        item: schema.object([rules.required]).members({
          _id: schema.string({ trim: true }) 
        }),
      })
    })
  }

  public async destroy() {

  }
}
