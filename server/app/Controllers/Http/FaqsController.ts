import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Faq from 'App/Models/Faq'

export default class FaqsController {
  public async index({ response }: HttpContextContract) {
    const faqs = await Faq.find().sort({ sort: 'desc' })

    return response.json({
      data: faqs
    })
  }

  public async store({ auth, request, response }: HttpContextContract) {
    const faq = await Faq.create({
      user: auth.user.id,
      session_id: request.input('session_id'),
      content: '',
      order: 0
    })

    return response.json({
      data: faq
    })
  }

  public async update({ request, params, response }) {
    await Faq.where({ id: params.faq }).update({
      content: request.input('content')
    })
  }

  public async destroy({ params }: HttpContextContract) {
    Faq.deleteOne({ id: params.faq })
  }
}
