import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Ws from 'App/Services/Ws'
import Item from 'App/Models/Item'
import got from 'got'

export default class ItemsController {
  public async index({ response }: HttpContextContract) {
    const items = await Item.find().sort('_id')

    return response.json(items)
  }

  public async store({ request, response }) {
    const data = request.only(['title', 'name', 'type', 'url'])
    Ws.io.emit('items:store', data)
    if (data.type === 'VIDEO')
      data.media = await this.extractMediaRoute(data.url)

    try {
      const item = new Item({
        ...data
      })
      
      await item.save()

      Ws.io.emit('items:store', item)
      return response.json(item)
    } catch (e) {
      console.log(e)
    }
  }

  public async show({ params, response }: HttpContextContract) {
    const session = await Item.findOne({ _id: params.item })

    return response.json({
      data: session
    })
  }

  public async update({ params, response, request }: HttpContextContract) {
    const session = await Item.findByIdAndUpdate({ _id: params.item }, request.only(['title', 'name', 'type', 'url']))

    Ws.io.emit('items:update', session)

    return response.json(session)
  }

  public async destroy({ params }: HttpContextContract) {
    await Item.findByIdAndDelete({ id: params.item })

    Ws.io.emit('items:destory', { id: params.item })
  }

  private async extractMediaRoute(defaultRoute):Promise<string> {
    const availableMediaRoutes = {
      auto: defaultRoute
    };
    const strippedMediaRoute = defaultRoute.replace(".m3u8", "");
    const routes = [
      {
        quality: "480",
        src: `${strippedMediaRoute}_480p/index.m3u8`
      },
      {
        quality: "720",
        src: `${strippedMediaRoute}_720p/index.m3u8`
      },
      {
        quality: "1080",
        src: `${strippedMediaRoute}_1080p/index.m3u8`
      }
    ];
    await Promise.all(
      routes.map(async route => {
        try {
          await got(route.src);
          availableMediaRoutes[route.quality] = route.src;
        } catch (error) {
          console.error("Media Route not available!:", route.src);
        }
      })
    );

    return JSON.stringify(availableMediaRoutes);
  }
}
