import { Schema, model } from '@ioc:Mongoose'

export default model('Chat',
  new Schema({
    name: String,
  })
)