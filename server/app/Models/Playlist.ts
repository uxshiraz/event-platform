import { Schema, model } from '@ioc:Mongoose'

export default model('Playlist', new Schema({
  item: {
    type: Schema.Types.ObjectId,
    ref: 'Item'
  },
  order: Number,
  is_active: Boolean
}))