import { Schema, model } from '@ioc:Mongoose'

export default model('Faq', new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  playlist: {
    type: Schema.Types.ObjectId,
    ref: 'Playlist'
  }
}))