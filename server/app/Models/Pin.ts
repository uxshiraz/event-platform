import { Schema, model } from '@ioc:Mongoose'

export default model('Pin', new Schema({
  title: String,
  logo: String,
  url: String  
}))