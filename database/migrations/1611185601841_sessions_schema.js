'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SessionsSchema extends Schema {
  up () {
    this.create('sessions', (table) => {
      table.increments()
      table.string('title')
      table.boolean('has_faq')
      table.integer('speaker_id').unsigned()
      table.integer('executor_id').unsigned()
      table.timestamp('started_at')
      table.timestamp('ended_at')
      table.string('type')
      table.string('media').nullable()
      table.boolean('is_active').nullable();
      table.string('state').nullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('sessions')
  }
}

module.exports = SessionsSchema
