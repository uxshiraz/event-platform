'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PinSchema extends Schema {
  up () {
    this.create('pins', (table) => {
      table.increments()
      table.string('title')
      table.string('thumb_url');
      table.string('link');
      table.integer('order').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('pins')
  }
}

module.exports = PinSchema
