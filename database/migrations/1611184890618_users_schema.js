'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.integer('id').unsigned();
      table.string('name');
      table.string('profile_link');
      table.string('avatar').nullable();
      table.integer('ticket_id').unsigned();
      table.string('profession').nullable();
      table.string('group').nullable();
      table.string('email');
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UsersSchema
