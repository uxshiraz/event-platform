const Ws = use("Ws");

Ws.channel('chat', 'ChatController').middleware(['auth'])
Ws.channel('question', 'FaqController').middleware(['auth']);
Ws.channel('session', 'SessionController').middleware(['auth'])
