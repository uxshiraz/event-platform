'use strict'

const { hooks } = require('@adonisjs/ignitor')

hooks.after.httpServer(() => {
  const Response = use('Adonis/Src/Response');

  Response.macro('flash', function (data, cookieOptions = { path: '/' }) {
    this.plainCookie('flash-data', JSON.stringify(data), cookieOptions);
  })

  Response.macro('flashError', function (error) {
    this.flash(JSON.stringify({
      type: 'error',
      text: error
    }));
  })
})