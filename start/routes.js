"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use("Route");

Route.group(() => {
  Route.post("/signin", "AuthController.authentication");
  Route.get("/me", "AuthController.me").middleware(["auth"]);
}).prefix("/auth");

Route.group(() => {
  /* signup guest user */
  Route.post('/signup', 'AuthController.signup');

  /* start chat routes */
  Route.get("/chat", "ChatController.index");
  Route.get("/chat/pin", "ChatController.pin");
  Route.post("/chat/old", "ChatController.fetchOld");
  Route.delete("/chat/:id", "ChatController.destroy");
  /* end chat routes */

  /* start faq routes */
  Route.get("/faq", "FaqController.index");
  Route.post("/faq", "FaqController.store");
  Route.put("/faq/:id", "FaqController.update");
  Route.delete("/faq/:id", "FaqController.destroy");
  /* end faq routes */

  /* start sessions route */
  Route.get('/current-session', 'SessionController.currentSession');
  Route.resource('/sessions', 'SessionController').apiOnly();

  Route.patch('/sessions/:id/archive', 'SessionController.archive')
  Route.post('/sessions/:id/clone', 'SessionController.clone')
  /* end sessions route */

  /* start pin route */
  Route.resource("/pins", "PinController").apiOnly();
  /* end pin route */

  /* start users route */
  Route.get("/users", "UserController.index");
  /* end users route */
}).prefix("/api");

Route.any("*", "NuxtController.render");
