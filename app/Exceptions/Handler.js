"use strict";

const BaseExceptionHandler = use("BaseExceptionHandler");
const sentry = use("Sentry");
const Env = use("Env");

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  report(error, { request }) {
    if (Env.get("SENTRY_ENABLED", false)) {
      sentry.captureException(error);
    }
  }
}

module.exports = ExceptionHandler;
