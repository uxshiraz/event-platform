'use strict'

/**
* Pseudo-random string generator
* http://stackoverflow.com/a/27872144/383904
* Default: return a random alpha-numeric string
* 
* @param {Integer} len Desired length
* @param {String} an Optional (alphanumeric), "a" (alpha), "n" (numeric)
* @return {String}
*/

const randomString = function(len, an) {
  an = an && an.toLowerCase();
  const min = an === "a" ? 10 : 0;
  const max = an === "n" ? 10 : 62;

  let str = "";
  let i = 0;

  for (; i++ < len;) {
    let r = Math.random() * (max - min) + min << 0;
    str += String.fromCharCode(r += r > 9 ? r < 36 ? 55 : 61 : 48);
  }

  return str;
}

module.exports = randomString;
