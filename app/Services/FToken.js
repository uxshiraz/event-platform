"use strict";

/**
 * @return {String}
 */

const FToken = function() {
  const s1 = (Math.floor(Math.random() * 9) + 1) * 100;
  const s2 = (Math.floor(Math.random() * 9) + 1) * 100;

  const d = Math.floor(Math.random() * 9) * 10;

  const y1 = Math.floor(Math.random() * 9);
  const y2 = Math.floor(Math.random() * 9);

  return s1 + d + y1 + "-" + (s2 + d + y2);
};

module.exports = FToken;
