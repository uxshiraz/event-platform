"use strict";

const BumblebeeTransformer = use("Bumblebee/Transformer");

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class UserTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */
  transform(model) {
    const user = model.toJSON();
    const users = process.env.ROOT_USER ? process.env.ROOT_USER.split(",") : [];

    return {
      _id: user._id,
      id: user.id,
      name: user.name,
      profile: user.profile_link,
      email: user.email,
      ticket_id: user.ticket_id,
      profession: user.profession,
      group: user.group,
      avatar: user.avatar ? JSON.parse(user.avatar) : null,
      is_admin: users.includes(user._id.toString()) ? true : false
    };
  }
}

module.exports = UserTransformer;
