'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * PinTransformer class
 *
 * @class PinTransformer
 * @constructor
 */
class PinTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */
  transform (model) {
    return {
      id: model._id,
      title: model.title,
      thumb_url: model.thumb_url,
      link: model.link,
      order: model.order,
      created_at: model.created_at
    }
  }
}

module.exports = PinTransformer
