"use strict";

const BumblebeeTransformer = use("Bumblebee/Transformer");

/**
 * FaqTransformer class
 *
 * @class FaqTransformer
 * @constructor
 */
class FaqTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */
  transform(model) {
    return {
      id: model._id,
      content: model.content,
      session_id: model.session_id,
      is_answered: Boolean(model.is_answered),
      is_archived: Boolean(model.is_archived),
      order: model.order,
      userId: model.user_id
    };
  }
}

module.exports = FaqTransformer;
