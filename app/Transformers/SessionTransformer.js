"use strict";

const BumblebeeTransformer = use("Bumblebee/Transformer");

/**
 * SessionTransformer class
 *
 * @class SessionTransformer
 * @constructor
 */
class SessionTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */

  transform(model) {
    const startDate = new Date(model.started_at);
    const now = new Date();
    const spendTime = now - startDate;
    const isVideo = model.type === "VIDEO";

    return {
      id: model._id,
      title: model.title,
      has_faq: Boolean(model.has_faq),
      speaker: model.speaker,
      type: model.type,
      media: isVideo ? JSON.parse(model.media) : model.media,
      is_active: Boolean(model.is_active),
      state: model.state,
      spend_time: spendTime,
      is_archived: Boolean(model.archived_at)
    };
  }
}

module.exports = SessionTransformer;
