"use strict";

const BumblebeeTransformer = use("Bumblebee/Transformer");

/**
 * ChatTransformer class
 *
 * @class ChatTransformer
 * @constructor
 */
class ChatTransformer extends BumblebeeTransformer {
  static get defaultInclude() {
    return ["parent", "user"];
  }
  /**
   * This method is used to transform the data.
   */

  transform(message) {
    return {
      id: message._id,
      content: message.content,
      created_at: message.created_at
    };
  }

  includeParent(message) {
    return this.item(message.getRelated("parent"), "ChatTransformer");
  }

  includeUser(message) {
    return this.item(message.getRelated("user"), "UserTransformer");
  }
}

module.exports = ChatTransformer;
