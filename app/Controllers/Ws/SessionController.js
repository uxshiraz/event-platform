"use strict";

const Session = use("App/Models/Session");

class SessionController {
  constructor({ socket, request, auth, transform }) {
    this.socket = socket;
    this.request = request;
    this.auth = auth;
    this.transform = transform;
  }

  async onActivation(data) {
    const prevActiveSession = await Session.where({ is_active: true }).first();

    if (prevActiveSession) {
      prevActiveSession.merge({
        is_active: false,
        state: null
      });
      await prevActiveSession.save();

      const transform = await this.transform.item(
        prevActiveSession,
        "SessionTransformer"
      );

      this.socket.broadcastToAll("deactive", transform);
    }

    const session = await Session.where({ _id: data.id }).first();

    if (!session) {
      return this.socket.emit("error", session);
    }

    session.merge({
      is_active: true,
      started_at: new Date(),
      state: null
    });

    await session.save();

    const transforme = await this.transform.item(session, "SessionTransformer");

    return this.socket.broadcastToAll("activation", transforme);
  }

  async onDeactive(data) {
    const session = await Session.where({ _id: data.id }).first();

    const isActive = session.is_active;

    session.merge({
      is_active: false
    });

    await session.save();

    if (isActive) {
      const transform = await this.transform.item(
        session,
        "SessionTransformer"
      );
      return this.socket.broadcastToAll("deactive", transform);
    }
  }

  async onNext() {
    const session = await Session.where({ is_active: true }).first();

    if (!session) return this.socket.emit("error", "no session");

    session.merge({
      is_active: false
    });
    await session.save();

    this.socket.broadcastToAll(
      "deactive",
      await this.transform.item(session, "SessionTransformer")
    );

    const nextSession = await Session.where("_id", ">", session._id).first();

    nextSession.merge({
      is_active: true,
      started_at: new Date(),
      state: null
    });

    await nextSession.save();

    this.socket.broadcastToAll(
      "next",
      await this.transform.item(nextSession, "SessionTransformer")
    );
  }

  async onQuality(data) {
    const session = await Session.where({ _id: data.id }).first();

    return this.socket.emit(
      "spent",
      await this.transform.item(session, "SessionTransformer")
    );
  }

  async onState(data) {
    const session = await Session.where({ _id: data.id }).first();

    if (!session) return this.socket.emit("error", "NotFound");

    session.merge({
      state: data.state
    });

    await session.save();

    return this.socket.broadcastToAll(
      "state",
      await this.transform.item(session, "SessionTransformer")
    );
  }
}

module.exports = SessionController;
