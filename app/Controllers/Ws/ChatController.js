"use strict";
global.onlineUsers = [];
global.pinActionUser = {};

const Chat = use("App/Models/Chat");
const User = use("App/Models/User");

class ChatController {
  constructor({ socket, request, transform, auth }) {
    this.socket = socket;
    this.request = request;
    this.transform = transform;
    this.auth = auth;
  }

  async onList({ user, limit }) {
    limit = limit || 20;

    if (user) {
      this.addOnlineUser(user);
    }

    const messageConut = await Chat.count();

    let messages;
    if (messageConut - limit <= 0) {
      messages = await Chat.sort({ created_at: 1 }).fetch();
    } else {
      messages = await Chat.sort({ created_at: 1 })
        .skip(messageConut - limit)
        .fetch();
    }

    const broadCastableMessages = await this.transform.collection(
      messages,
      "ChatTransformer"
    );

    return this.socket.emit("list", {
      data: broadCastableMessages.data,
      onlineUsers: global.onlineUsers
    });
  }

  async onMessage(data) {
    const user = await this.auth.getUser();

    try {
      const message = await Chat.create({
        user_id: user._id,
        content: data.content,
        parent_id: data.parent_id ? data.parent_id : null,
        is_pinned: false
      });

      const broadCastableMessage = await this.transform
        .include("user,parent")
        .item(message, "ChatTransformer");

      return this.socket.broadcastToAll("message", {
        data: broadCastableMessage.data,
        onlineUsers: global.onlineUsers
      });
    } catch (e) {}
  }

  async onPin(data) {
    try {
      // Get teh user that has pinned the message
      const user = await User.where({ _id: data.userId }).first();
      global.pinActionUser = user;
      // Removing old pinned message
      const oldPin = await Chat.where({ is_pinned: true }).first();

      if (oldPin) {
        oldPin.merge({ is_pinned: false });
        await oldPin.save();
      }
      // Pinning new message
      const id = data ? data.id : null;
      const newPin = await Chat.where({ _id: id }).first();
      if (newPin) {
        newPin.merge({ is_pinned: true });
        await newPin.save();
      }
      const broadCastablePin = await this.transform.item(
        newPin,
        "ChatTransformer"
      );

      return this.socket.broadcastToAll("pin", {
        data: broadCastablePin ? broadCastablePin.data : null,
        user
      });
    } catch (e) {}
  }

  async onClose() {
    const user = await this.auth.getUser();
    this.removeOnlineUser(user._id);
  }

  addOnlineUser(user) {
    if (global.onlineUsers.filter(item => item._id === user._id).length > 0)
      return;
    global.onlineUsers.push(user);
  }

  removeOnlineUser(userId) {
    global.onlineUsers = global.onlineUsers.filter(item => item._id !== userId);
  }
}

module.exports = ChatController;
