"use strict";

const Faq = use("App/Models/Faq");
const { validate } = use("Validator");

class FaqController {
  constructor({ socket, request, auth, transform }) {
    this.socket = socket;
    this.request = request;
    this.auth = auth;
    this.transform = transform;
  }

  async onList(data) {
    const faqs = await Faq.where({ session_id: data.id })
      .sort({ sort: 1 })
      .fetch();

    return this.socket.emit(
      "list",
      await this.transform.collection(faqs, "FaqTransformer")
    );
  }

  async onArchiveList(data) {
    const faqs = await Faq.where({ session_id: data.id })
      .where({ is_archived: true })
      .sort({ sort: 1 })
      .fetch();

    return this.socket.emit(
      "archiveList",
      await this.transform.collection(faqs, "FaqTransformer")
    );
  }

  async onMessage(data) {
    const validation = await validate(data, {
      content: "required|string",
      session_id: "required"
    });

    const { session_id, content } = data;

    try {
      const user = await this.auth.getUser();

      const faq = await Faq.create({
        user_id: user._id,
        session_id,
        content,
        is_answered: false,
        is_archived: false,
        sort: 0
      });

      return this.socket.broadcastToAll(
        "message",
        await this.transform.item(faq, "FaqTransformer")
      );
    } catch (e) {
      this.socket.emit("error", e);
    }
  }

  async onUpdate(data) {
    const faq = await Faq.where({ _id: data.id }).first();

    faq.merge({
      content: data.content
    });

    await faq.save();

    return this.socket.broadcastToAll(
      "update",
      await this.transform.item(faq, "FaqTransformer")
    );
  }

  async onDestroy(data) {
    const faq = await Faq.where({ _id: data.id }).first();
    await faq.delete();

    return this.socket.broadcastToAll(
      "destroy",
      await this.transform.item(faq, "FaqTransformer")
    );
  }

  async onAnswered(data) {
    const faq = await Faq.where({ _id: data.id }).first();

    faq.merge({
      is_answered: true
    });

    await faq.save();

    this.socket.broadcastToAll(
      "update",
      await this.transform.item(faq, "FaqTransformer")
    );
  }

  async onUnAnswered(data) {
    const faq = await Faq.where({ _id: data.id }).first();

    faq.merge({
      is_answered: false
    });

    await faq.save();

    this.socket.broadcastToAll(
      "update",
      await this.transform.item(faq, "FaqTransformer")
    );
  }

  async onArchived(data) {
    const faq = await Faq.where({ _id: data.id }).first();

    faq.merge({
      is_archived: true
    });

    await faq.save();

    return this.socket.broadcastToAll(
      "update",
      await this.transform.item(faq, "FaqTransformer")
    );
  }

  async onUnArchived(data) {
    const faq = await Faq.where({ _id: data.id }).first();

    faq.merge({
      is_archived: false
    });

    await faq.save();

    return this.socket.broadcastToAll(
      "update",
      await this.transform.item(faq, "FaqTransformer")
    );
  }

  async onSort({ questions, sessionId }) {
    await Promise.all(
      questions.map(async item => {
        try {
          const faq = await Faq.where({ _id: item.id }).first();
          faq.merge({
            sort: item.sort
          });
          await faq.save();
        } catch (error) {
          console.error("Error updating question sort");
        }
      })
    );

    const allQuestions = await Faq.where({ session_id: sessionId })
      .sort({ sort: 1 })
      .fetch();

    this.socket.broadcastToAll(
      "list",
      await this.transform.collection(allQuestions, "FaqTransformer")
    );

    const archivedQuestions = await Faq.where({ session_id: sessionId })
      .where({ is_archived: true })
      .sort({ sort: 1 })
      .fetch();

    return this.socket.broadcastToAll(
      "archiveList",
      await this.transform.collection(archivedQuestions, "FaqTransformer")
    );
  }
}

module.exports = FaqController;
