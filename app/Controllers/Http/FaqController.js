"use strict";

const Faq = use("Faq");
const { validate } = use("Validator");

class FaqController {
  async index({ transform }) {
    const faq = await Faq.orderBy("sort", "desc").get();

    // TODO send socket
    return transform.collection(faq, "FaqTransformer");
  }

  async store({ request, transform, response, auth }) {
    const validation = await validate(request.all(), {
      content: "required|string",
      session_id: "required"
    });

    if (validation.fails())
      return response.status(422).json({
        error: 422,
        messages: validation.messages()
      });

    const { session_id, content } = request.all();

    const faq = await Faq.create({
      user_id: auth.user.id,
      session_id,
      content,
      order: 0
    });

    // TODO convert to socket
    return transform.item(faq, "FaqTransformer");
  }

  async update({ request, params, response, transform }) {
    const validation = await validate(request.all(), {
      content: "required|string"
    });

    const { content } = request.all();

    if (validation.fails())
      return response.status(422).json({
        error: 422,
        messages: validation.messages()
      });

    const faq = await Faq.where({ id: params.id }).first();

    faq.merge({
      content
    });

    await faq.save();

    // TODO convert to socket
    return transform.item(faq, "FaqTransform");
  }

  destroy({ params }) {
    Faq.where({ id: params.id }).delete();

    // TODO send socket message
  }
}

module.exports = FaqController;
