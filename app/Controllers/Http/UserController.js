"use strict";

const User = use("App/Models/User");

class UserController {
  async index({ transform }) {
    const users = await User.fetch();

    return transform.collection(users, "UserTransformer");
  }
}

module.exports = UserController;
