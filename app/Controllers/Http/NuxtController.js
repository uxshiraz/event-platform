'use strict'

class NuxtController {
  constructor() {
    this.nuxt = use('Service/Nuxt')
  }

  async render({ request: { request: req }, response, session }) {
    response.implicitEnd = false
    await session.commit()

    await new Promise((resolve, reject) => {
      this.nuxt.render(req, response.response, (promise) => {
        promise.then(resolve).catch(reject)
      })
    })
  }
}

module.exports = new NuxtController()
