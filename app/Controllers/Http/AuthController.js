"use strict";

const got = use("got");
const gravatar = use('gravatar');
const { validate } = use("Validator");
const User = use("App/Models/User");
const Hash = use('Hash');

class AuthController {
  /**
   * authenticate the user with third party website and
   * return the token to the front end
   * @param {request}
   * @param {response}
   * @param {auth}
   */
  async authentication({ request, response, auth }) {
    const { type } = request.all();

    console.log(type)
    switch (type) {
      case 'guest':
        await this.authenticateForm(request, response, auth);
        break;
      default:
        await this.authenticateToken(request, response, auth);
        break;
    }
  }

  async authenticateToken(request, response, auth) {
    const validation = await validate(request.all(), {
      token: "required|string"
    });

    if (validation.fails())
      return response.status(401).json({
        error: 401,
        messages: validation.messages()
      });

    const { token } = request.all();

    await got.get(process.env.HOST_AUTH_USER, {
        headers: {
          Authorization: token
        },
        responseType: "json"
      }).then(async res => {
        const userInfo = res.body.data;

        const data = {
          id: userInfo.user_id,
          name: userInfo.name,
          profile_link: userInfo.profile,
          email: userInfo.email,
          avatar: JSON.stringify({
            avatar: userInfo.avatar,
            avatar_md: userInfo.avatar_md,
            avatar_lg: userInfo.avatar_lg
          }),
          ticket_id: userInfo.ticket_id,
          profession: userInfo.profession,
          group: userInfo.group
        };

        let user = await User.where({ id: userInfo.user_id }).first();

        if (!user) {
          user = new User();

          user.fill(data);
        } else 
          user.merge(data);

        await user.save();

        const token = await auth.generate(user);

        return response.json({
          code: "200",
          data: token
        });
      })
      .catch(error => {
        if (error.response) {
          return response.status(401).json({
            error: error.response.statusCode,
            message: error
          });
        }

        return response.status(500).json({
          error: 500,
          message: error
        })
      });
  }

  async authenticateForm(request, response, auth) {
    const validation = await validate(request.all(), {
      email: 'required',
      password: 'required|min:6'
    })

    if (validation.fails())
      return response.status(422).json({
        error: 422,
        messages: validation.messages()
      })

    const { email, password } = request.all();

    const token = await auth.attempt(email, password);

    return response.status(200).json({
      code: 200,
      data: token
    })
  }

  async signup({ request, response, auth }) {
    const validation = await validate(request.all(), {
      name: 'required|string',
      email: 'required|email|unique:users,email',
      password: 'required|min:6',
    })

    if (validation.fails())
      return response.status(422).json({
        error: 422,
        messages: validation.messages()  
      })
    
    const { email, password, name } = request.all()
    
    const avatar = await gravatar.url(email, { s: 200 })

    const user = new User();

    user.fill({
      name,
      email,
      avatar: JSON.stringify({
        avatar: avatar,
        avatar_md: avatar,
        avatar_lg: avatar
      })
    })
    user.password = await Hash.make(password)
    await user.save()

    return response.status(200).json({
      code: 200,
      data: user
    })
  }

  async me({ auth, transform }) {
    return await transform.item(auth.user, "UserTransformer");
  }
}

module.exports = AuthController;
