"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const got = use("got");
const Session = use("App/Models/Session");
const { validate } = use("Validator");

/**
 * Resourceful controller for interacting with sessions
 */
class SessionController {
  /**
   * Show a list of all sessions.
   * GET sessions
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ transform }) {
    const sessions = await Session.query().whereNull('archived_at').sort("created_at").fetch();

    return transform.collection(sessions, "SessionTransformer");
  }

  /**
   * fetch the archived sessions
   * 
   * @param {Object} ctx
   */
  async fetchArchived({ transform }) {
    const sessions = await Session.query().whereNotNull('archived_at').sort('created_at').fetch()

    return transform.collection(sessions, "SessionTransformer");
  }

  /**
   * Create/save a new session.
   * POST sessions
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, transform }) {
    const validation = await validate(request.all(), {
      title: "required|string",
      has_faq: "required|boolean",
      type: "required|in:LIVE,VIDEO,AUDIO"
    });

    if (validation.fails())
      return response.status(422).json({
        error: 422,
        messages: validation.messages()
      });

    const data = request.only([
      "title",
      "has_faq",
      "speaker",
      "type",
      "media"
    ]);

    if (data.type === "VIDEO") {
      data.media = await this.extractMediaRoutes(data.media);
    }

    const session = await Session.create({
      ...data,
      started_at: null,
      is_active: false,
      state: null
    });

    return transform.item(session, "SessionTransformer");
  }

  async extractMediaRoutes(defaultRoute) {
    const availableMediaRoutes = {
      auto: defaultRoute
    };
    const strippedMediaRoute = defaultRoute.replace(".m3u8", "");
    const routes = [
      {
        quality: "480",
        src: `${strippedMediaRoute}_480p/index.m3u8`
      },
      {
        quality: "720",
        src: `${strippedMediaRoute}_720p/index.m3u8`
      },
      {
        quality: "1080",
        src: `${strippedMediaRoute}_1080p/index.m3u8`
      }
    ];
    await Promise.all(
      routes.map(async route => {
        try {
          await got(route.src);
          availableMediaRoutes[route.quality] = route.src;
        } catch (error) {
          console.error("Media Route not available!:", route.src);
        }
      })
    );
    return JSON.stringify(availableMediaRoutes);
  }

  /**
   * Display a single session.
   * GET sessions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, transform }) {
    const session = await Session.where({ id: params.id }).first();

    return transform.item(session, "SessionTransform");
  }

  /**
   * Update session details.
   * PUT or PATCH sessions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response, transform }) {
    const validation = await validate(request.all(), {
      title: "required|string",
      has_faq: "required|boolean",
      type: "required|in:LIVE,VIDEO,AUDIO"
    });

    if (validation.fails())
      return response.status(422).json({
        error: 422,
        messages: validation.messages()
      });

    const session = await Session.where({ id: params.id }).first();

    const data = request.only([
      "title",
      "has_faq",
      "speaker",
      "type",
      "media"
    ]);
    session.merge(data);

    await session.save();

    return transform.item(session, "SessionTransformer");
  }

  /**
   * Delete a session with id.
   * DELETE sessions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, transform }) {
    const session = await Session.where({ _id: params.id }).first();

    session.delete()

    return transform.item(session, "SessionTransformer");
  }

  async currentSession({ transform, response }) {
    const session = await Session.where({ is_active: true }).first();

    if (!session) return { data: null };

    return transform.item(session, "SessionTransformer");
  }

  /**
   * Archive the session
   * 
   * @param {Pramas}  ctx.params
   */
  async archive({ params, transform }) {
    const session = await Session.where({ _id: params.id }).first();

    session.merge({
      archived_at: new Date()
    })

    await session.save()

    return transform.item(session, 'SessionTransformer')
  }

  /**
   * Create a clone from session
   * 
   * @param {Params} ctx.params
   */
  async clone({ params, transform }) {
    const session = await Session.where({ _id: params.id }).first();

    const newSession = await Session.create({
      title: session.title,
      speaker: session.speaker,
      has_faq: session.has_faq,
      media: session.media,
      type: session.type,
      started_at: null,
      is_active: false,
      state: null
    })

    return transform.item(newSession, 'SessionTransformer')

  }
}

module.exports = SessionController;
