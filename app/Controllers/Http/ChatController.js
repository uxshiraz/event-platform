"use strict";

const Chat = use("App/Models/Chat");

class ChatController {
  index({ transform }) {
    const chat = Chat.orderBy("id", "desc")
      .with(["parent", "user"])
      .paginate(20);

    return transform.include("user,parent").collection(chat, "ChatTransform");
  }

  async destroy({ params }) {
    await Chat.where({ id: params.id }).delete();

    // broadcast message
  }

  async pin({ transform }) {
    try {
      const pinnedMessage = await Chat.where({ is_pinned: true }).first();

      const broadCastablePin = await transform.item(
        pinnedMessage,
        "ChatTransformer"
      );
      return {
        data: broadCastablePin ? broadCastablePin.data : null,
        user: global.pinActionUser
      };
    } catch (e) {
      return e;
    }
  }

  async fetchOld({ request, transform }) {
    const { messageId } = request.all();
    let { limit } = request.all();
    limit = limit || 20;

    try {
      const messages = await Chat
        .where({ _id: { $lt: messageId } })
        .sort({ created_at: -1 })
        .limit(limit)
        .fetch();

      const transformedMessages = await transform.collection(
        messages,
        "ChatTransformer"
      );

      let endOfMessages = false;
      if (transformedMessages.data.length < limit) endOfMessages = true;
      return { messages: transformedMessages.data, end: endOfMessages };
    } catch (e) {
      console.log(e)
      return e;
    }
  }
}

module.exports = ChatController;
