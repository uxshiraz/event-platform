"use strict";

const Pin = use("App/Models/Pin");
const { validate } = use("Validator");

class PinController {
  /**
   * Show a list of all pins.
   * GET pins
   *
   */
  async index({ transform }) {
    const pins = await Pin.sort("-order").fetch();

    return transform.collection(pins, "PinTransformer");
  }

  /**
   * Create/save a new pins.
   * POST pins
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, transform }) {
    const validation = await validate(request.all(), {
      title: "required",
      thumb_url: "required",
      link: "required"
    });

    if (validation.fails())
      return response.status(422).json({
        status: 422,
        messages: validation.messages()
      });

    const pin = await Pin.create(
      request.only(["title", "thumb_url", "link", "order"])
    );

    return transform.item(pin, "PinTransformer");
  }

  /**
   * Update pin details.
   * PUT pins/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ request, params, response, transform }) {
    const validation = await validate(request.all(), {
      title: "required",
      thumb_url: "required",
      link: "required"
    });

    if (validation.fails())
      return response.status(422).json({
        status: 422,
        messages: validation.messages()
      });

    const pin = await Pin.where({ _id: params.id }).firstOrFail();
    pin.merge(request.only(["title", "thumb_url", "link", "order"]));

    await pin.save();

    return transform.item(pin, "PinTransformer");
  }

  /**
   * Delete a session with id.
   * DELETE pins/:id
   *
   * @param {object} ctx
   */
  async destroy({ params, response }) {
    await Pin.where({ _id: params.id }).delete();

    return response.status(200);
  }
}

module.exports = PinController;
