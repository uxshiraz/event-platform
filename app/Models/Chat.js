"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Chat extends Model {
  user() {
    return this.belongsTo("App/Models/User", "user_id", "_id");
  }

  parent() {
    return this.hasOne("App/Models/Chat", "parent_id", "_id").with("user");
  }
}

module.exports = Chat;
