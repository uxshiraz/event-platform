"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Session extends Model {
  static get objectIdFields() {
    return ["_id"];
  }

  static get dates() {
    return ["started_at", "ended_at", 'archived_at'];
  }

  static get dateFields() {
    return [
      'started_at',
      'ended_at',
      'archived_at'
    ];
  }
}

module.exports = Session;
