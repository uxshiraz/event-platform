'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class WsBumblebeeProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register () {
    //
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot () {
    const WsContext = this.app.use('Adonis/Addons/WsContext')
    const Bumblebee = this.app.use('Adonis/Addons/Bumblebee')

    WsContext.getter('transform', function() {
      return Bumblebee.create().withContext(this)
    }, true)
  }
}

module.exports = WsBumblebeeProvider
