const { resolve } = require("path");

module.exports = {
  dev: process.env.NODE_ENV === "development",
  srcDir: resolve(__dirname, "..", "resources"),
  telemetry: false,

  ssr: false,

  env: {
    appUrl: process.env.APP_URL,
    authUrl: process.env.HOST_AUTH_LOGIN,
    nodeEnv: process.env.NODE_ENV,
    cdnEnabled: process.env.CDN_ENABLED,
    cdnUrl: process.env.CDN_URL,
    hostCookieName: process.env.HOST_AUTH_COOKIE_NAME,
    liveUri: process.env.LIVE_URI,
    rootUsers: process.env.ROOT_USER,
    developeToken: process.env.DEVELOPE_TOKEN 
  },

  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: "fa",
      dir: "rtl"
    },
    title: "پلتفرم رویداد یواکس شیراز",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "msapplication-TileColor",
        name: "msapplication-TileColor",
        content: "#ffffff"
      },
      {
        hid: "msapplication-config",
        name: "msapplication-config",
        content: "/assets/icons/browserconfig.xml?v=m2nOm7oR7w"
      },
      { hid: "theme-color", name: "theme-color", content: "#f05730" },
      { hid: "og:locale", name: "og:locale", content: "fa_IR" },
      {
        hid: "og:site_name",
        name: "og:site_name",
        content: "یواکس شیراز"
      }
    ],
    link: [
      {
        rel: "apple-touch-icon",
        href: "/assets/icons/apple-touch-icon.png?v=m2nOm7oR7w",
        sizes: "180x180"
      },
      {
        rel: "icon",
        href: "/assets/icons/favicon-32x32.png?v=m2nOm7oR7w",
        type: "image/png",
        sizes: "32x32"
      },
      {
        rel: "icon",
        href: "/assets/icons/favicon-16x16.png?v=m2nOm7oR7w",
        type: "image/png",
        sizes: "16x16"
      },
      { rel: "manifest", href: "/assets/icons/site.webmanifest?v=m2nOm7oR7w" },
      {
        rel: "mask-icon",
        href: "/assets/icons/safari-pinned-tab.svg?v=m2nOm7oR7w",
        color: "#f05730"
      },
      {
        rel: "shortcut icon",
        href: "/assets/icons/favicon.ico?v=m2nOm7oR7w"
      }
    ]
  },

  router: {
    middleware: ["auth"]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#f05730" },
  /*
   ** Global CSS
   */
  css: ["@/assets/scss/app.scss"],

  styleResources: {
    scss: ["~assets/scss/_basic.scss"]
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "@/plugins/urls.js",
    // { src: '@/plugins/bootstrap-ui.js', ssr: true },
    "@/plugins/bootstrap-ui.js",
    "@/plugins/icons.js",
    "@/plugins/i18n.js",
    "@/plugins/toast.js",
    "@/plugins/filters.js",
    "@/plugins/moment.js",
    "@/plugins/ui-component",
    "@/plugins/audio-visual"
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    "@nuxtjs/eslint-module"
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    // "@nuxtjs/pwa",
    // Doc: https://github.com/nuxt-community/dotenv-module
    "@nuxtjs/dotenv",
    "@nuxtjs/auth",
    "@nuxtjs/style-resources",
    "cookie-universal-nuxt"
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.API_URL,
    withCredentials: true
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: `${process.env.AUTH_URL}/signin`,
            method: "post",
            propertyName: "data.token"
          },
          user: {
            url: `${process.env.AUTH_URL}/me`,
            method: "get",
            propertyName: "data"
          },
          logout: false
        }
      }
    },

    redirect: {
      // User will be redirected to this path if login is required.
      login: process.env.HOST_AUTH_LOGIN,
      // User will be redirected to this path if after logout, current route is protected.
      logout: '/',
      // User will be redirected to this path by the identity provider after login.
      // Should match configured Allowed Callback URLs (or similar setting)
      // in your app/client with the identity provider
      callback: false,
      // User will be redirect to this path after login.
      // (rewriteRedirects will rewrite this path)
      home: "/"
    },

    plugins: ["~/plugins/ws.js"]
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extractCSS: process.env.NODE_ENV === "production",

    extend(config, ctx) {}
  }
};
