"use strict";

const Env = use("Env");

module.exports = {
  /*
   |--------------------------------------------------------------------------
   | API key
   |--------------------------------------------------------------------------
   */
  dsn: Env.get("SENTRY_DSN"),

  environment: Env.get(
    "SENTRY_ENVIRONMENT",
    Env.get("NODE_ENV", "development")
  ),

  options: {
    // captureUnhandledRejections: true
  }
};
